unit module Local::Routes::Rand;

use Cro::HTTP::Router;
use Local::DB;

get -> 'rand' {
    redirect '/' ~ db.query(
        'SELECT slug FROM distinct_dists TABLESAMPLE SYSTEM_ROWS(1)').value;
}
