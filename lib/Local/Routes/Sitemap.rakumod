unit module Local::Routes::Sitemap;

use Cro::HTTP::Router;
use Local::DB;

get -> 'sitemap.xml' { content 'text/xml', db.query(q:to/SQL/).value }
    WITH urls AS (
        SELECT xmlelement(
                   name url,
                   xmlforest(
                       'https://raku.land/' || slug AS loc,
                       to_json(date)#>>'{}' || 'Z'  AS lastmod
                   )
               ) url
          FROM distinct_dists
      ORDER BY slug
    ) SELECT '<?xml version="1.0" encoding="UTF-8"?>' || xmlelement(
                 name urlset,
                 xmlattributes(
                     'http://www.sitemaps.org/schemas/sitemap/0.9' AS xmlns
                 ),
                 xmlagg(url)
             ) FROM urls
SQL
