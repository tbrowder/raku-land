unit module Local::Routes::Recent;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;
use Local::Pager;

constant $per-page = 50;

get -> 'recent', PosInt :$page = 1 {
    my @bind  = $per-page, ( $page - 1 ) * $per-page;
    my @dists = db.query( q:to/SQL/, |@bind ).hashes;
        SELECT date, description, name, slug, version,
               COUNT(*) OVER()
          FROM dists
      ORDER BY date DESC, name
         LIMIT $1 OFFSET $2
    SQL

    my $pager = Local::Pager.new
        :$page :$per-page :total(@dists[0]<count> // 0);

    template 'recent.crotmp', { :@dists :$pager };
}

get -> 'recent', 'json' {
    my @items = db.query( q:to/SQL/, $per-page ).hashes;
        SELECT 'https:' || a.avatar           author_avatar,
               a.name                         author_name,
               'https://raku.land/' || a.slug author_url,
               d.date                         date_published,
               d.description                  content_text,
               d.slug || '/' || d.version     id,
               d.name || ' ' || d.version     title,
               'https://raku.land/' || d.slug url
          FROM dists   d
          JOIN authors a ON d.author_id = a.id
      ORDER BY d.date DESC, d.name
         LIMIT $1
    SQL

    .<authors>[0] = %(
        avatar => .<author_avatar>:delete,
        name   => .<author_name  >:delete,
        url    => .<author_url   >:delete,
    ) for @items;

    content 'application/feed+json', {
        feed_url      => 'https://raku.land/recent/json',
        home_page_url => 'https://raku.land',
        items         => @items,
        title         => 'Recent Distributions',
        version       => 'https://jsonfeed.org/version/1.1',
    };
}
