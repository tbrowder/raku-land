unit module Local::Routes::Prolific;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;
use Local::Pager;

get -> 'prolific', PosInt :$page = 1 {
    constant $per-page = 25;

    my @bind    = $per-page, ( $page - 1 ) * $per-page;
    my @authors = db.query( q:to/SQL/, |@bind ).hashes;
        WITH dists AS (
            SELECT author_id, COUNT(*) OVER(), COUNT(*) dists,
                   RANK() OVER(ORDER BY COUNT(*) DESC)
              FROM distinct_dists
          GROUP BY author_id
          ORDER BY rank, author_id
             LIMIT $1 OFFSET $2
        ) SELECT avatar, name, slug, dists.*
            FROM dists
            JOIN authors ON author_id = authors.id
        ORDER BY rank, name
    SQL

    my $pager = Local::Pager.new
        :$page :$per-page :total(@authors[0]<count> // 0);

    template 'prolific.crotmp', { :@authors :$pager }
}
