need Cro::HTTP::Middleware;

unit class Local::Middleware::Log does Cro::HTTP::Middleware::RequestResponse;

method process-requests($supply) { $supply.do: { .annotations<start> = now } }

method process-responses($supply) {
    $supply.do: {
        printf "%s %d %s %s %.3fs %s\n",
            DateTime.now.hh-mm-ss,
            .status,
            .request.method,
            .request.target,
            now - .request.annotations<start>,
            .request.header: 'user-agent';
    }
}
