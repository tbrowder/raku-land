# Raku Land

This is the repository behind https://raku.land

## Roadmap

* API
* About/FAQ page
* Anchors on headers
* Atom/RSS feeds
* Blue/green deploy (zero downtime)
* Bus factor
* CI/CD
* De-dupe dists
* E2E tests
* Link deps between dists
* Render POD6
* Reverse deps
* River stats
* Search authors/tags
* Search faceting (ecosystem, tags, etc.)

## Quickstart

1. Install dependencies:
* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [make](https://www.gnu.org/software/make/) - most likely already on your system
* [mkcert](https://github.com/FiloSottile/mkcert#installation)

2. Install the local CA:
```
$ make cert
Using the local CA at "~/.local/share/mkcert" ✨
The local CA is now installed in the system trust store! ⚡️
The local CA is now installed in the Firefox and/or Chrome/Chromium trust store (requires browser restart)! 🦊


Created a new certificate valid for the following names 📜
 - "localhost"

The certificate is at "./localhost.pem" and the key at "./localhost-key.pem" ✅
```

3. Bring up the website:
```
$ make dev
```

4. Navigate to https://localhost

## Populating the database

The database is populated by a series of scripts to be run either
automatically or on-demand. These can be found under `bin`. The main entrypoint
is `ingest`, which orchestrates the process.

### Indeing distributions

In this directory, the `cpan` and the `zef` steps take snapshots of the
distributions available under the respective ecosystems. Distributions that
exist upstream are copied into a local cache, while any that may have been
deleted are removed. At the end, the local cache represents an exact
representation the distributions in these ecosystems.

### HTML rendering

After this, the `gmark` step processes all the `README.md` files and renders
them in HTML. We use [goldmark] for this and [Chroma] for syntax highlighting.

### Parsing meta-data

The final step is the `parse` step, where the contents of this local cache
(which now includes our HTML readmes) is processed and stored in the DB.

The data in the DB is completely replaced as part of this process, and this
is done in a single transaction. This means we do not store any state across
ingestions, which greatly simplifies things.

The parsing stage does the following:

* We fetch author information from both CPAN and zef. This includes most of
  the information later rendered in the author page.

* We process all the `META6.json` and `META.json` files available in our cache
  for distribution metadata, including names, versions, auths, provides, etc.

* We count the times each dist has been "starred" in the platforms we support.
  So far, this is only available for distributions hosted in Gitlab.

[Chroma]:   https://github.com/alecthomas/chroma
[goldmark]: https://github.com/yuin/goldmark
