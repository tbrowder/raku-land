#!/usr/bin/env raku

use DB::Pg;
use HTTP::Tiny;
use Local::GraphQL;
use JSON::Fast;
use TOML::Thumb;
use Zef::Distribution;

# Disable output buffering.
.out-buffer = False for $*ERR, $*OUT;

my %authors     = from-json get 'https://360.zef.pm/meta.json';
my %downloads   = from-json get 'https://360.zef.pm/stats.json';
my %tag-mapping = 'tag-mapping.toml'.IO.&from-toml.invert;

# TODO Unfortunately both XML & LibXML are too slow and too memory hungry.
for get("https://www.cpan.org/authors/00whois.xml").split("<cpanid>").tail(* - 1) {
    my %author = m:g/
        |       '<email>'       $<email> = .+? '</email>'
        |    '<fullname>'        $<name> = .+? '</fullname>'
        | '<has_cpandir>' $<has_cpandir> = .+? '</has_cpandir>'
        |    '<homepage>'     $<website> = .+? '</homepage>'
        |          '<id>'          $<id> = .+? '</id>'
        |  '<introduced>'        $<date> = .+? '</introduced>'
    /».hash».kv.flat».Str;

    if %author<has_cpandir>:delete {
        $_ = DateTime.new: +$_ with %author<date>;

        # Entity decode.
        .=trans: qw[&quot; &amp; &lt; &gt;] => qw[" & < >]
            for %author<email name>.grep: *.defined;

        %authors{ 'cpan:' ~ (%author<id>:delete) } = %author;
    }
}

# TODO Use GraphQL to get GitHub/GitLab author data.
%authors{"github:$_"} = %( :name($_) ) for dir("/cache/github")».basename;
%authors{"gitlab:$_"} = %( :name($_) ) for dir("/cache/gitlab")».basename;

for %authors.values {
    # Basic sanity check.
    .<email>:delete if .<email>:exists && .<email> !~~ / '@' /;

    with .<website> {
        # Add "http://" if we have no protocol.
        $_ = "http://$_" unless .contains: '://';

        # Remove trailing slashes from bare domains.
        s|^ ( 'http' 's'? '://' <-[/]>+ ) '/' $|$0|;
    }
}

( my $db = DB::Pg.new.db ).begin;

# Deleting authors will cascade into everything, reset auto-inc back to 1.
$db.execute: 'DELETE FROM authors';
$db.execute: 'ALTER SEQUENCE dists_id_seq RESTART';

my $insert-author = $db.prepare: q:to/SQL/;
    INSERT INTO authors (id, date, email, name, website)
                 VALUES ($1,   $2,    $3,   $4,      $5)
    ON CONFLICT DO NOTHING
SQL

my $insert-dep = $db.prepare: q:to/SQL/;
    INSERT INTO deps (dist_id, api, auth, "from", name, phase, ver)
              VALUES (     $1,  $2,   $3,     $4,   $5,    $6,  $7)
SQL

my $insert-dist = $db.prepare: q:to/SQL/;
    INSERT INTO dists
                (         api,     auth, author_id, changes,     date,
                  description, download, downloads,     eco, licenses,
                         name,     perl,  provides,  readme,     size,
                         tags,      url,   version)
         VALUES (          $1,       $2,        $3,      $4,       $5,
                           $6,       $7,        $8,      $9, $10::text[]::license[],
                          $11,      $12,       $13,     $14,      $15,
                          $16,      $17,       $18)
             -- Versions aren't always bumped, ignore conflicts, recent wins.
    ON CONFLICT (slug, version) DO NOTHING
      RETURNING id
SQL

for [
    |dir("/cache/cpan")».dir.flat,
    |dir("/cache/github")».dir.flat».dir.flat,
    |dir("/cache/gitlab")».dir.flat».dir.flat,
    |dir("/cache/zef").grep(*.d),
].sort(*.add("date").slurp).reverse -> $dir {
    CATCH { default { note "$dir: $_.message()" } }

    my $eco = $dir ~~ /^ '/cache/cpan/' / ?? 'CPAN'
           !! $dir ~~ /^ '/cache/git'   / ?? 'p6c'
           !! $dir ~~ /^ '/cache/zef/'  / ?? 'zef'
           !! ...;

    my $meta = <META6.json META.json META.info>.map({ $dir.add($_) }).first: *.e
        or die "Can't find a META file";

    my %meta = $meta.slurp.&from-json;
    my $auth = $eco eq 'zef'
        ?? %meta<auth> !! $dir.Str.split("/")[2,3].join: ':';

    # AlexDaniel/foo-emptystring  - How do we produce a URL for empty string?
    # AlexDaniel/foo-nullbyte-foo - PostgreSQL doesn't support NULL in strings
    next if %meta<name> ~~ '' | /\0/;

    my @licenses = do with %meta<license> {
        my constant %license-map = (
            ''                   => Empty,
            'AGPL-3.0'           => 'AGPL-3.0-only',
            'AND'                => Empty,
            'Apache-License-2.0' => 'Apache-2.0',
            'Artist-2.0'         => 'Artistic-2.0',
            'Artistic'           => 'Artistic-2.0',
            'BSD-3'              => 'BSD-3-Clause',
            'GPL'                => 'GPL-3.0-only',
            'GPL-1.0+'           => 'GPL-1.0-or-later',
            'GPL-2.0'            => 'GPL-2.0-only',
            'GPL-2.0+'           => 'GPL-2.0-or-later',
            'GPL-3.0'            => 'GPL-3.0-only',
            'LGPL-2.1'           => 'LGPL-2.1-only',
            'LGPL-3.0+'          => 'LGPL-3.0-or-later',
            'NOASSERTION'        => Empty,
            'NONE'               => Empty,
            'OR'                 => Empty,
            'Perl'               => 'Artistic-1.0-Perl',
            'artistic_2'         => 'Artistic-2.0',
            'gpl-3.0'            => 'GPL-3.0-only',
            'mit'                => 'MIT',
            'perl_5'             => 'Artistic-1.0-Perl',
            'http://www.perlfoundation.org/artistic_license_2_0' => 'Artistic-2.0',
        );

        # Map invalid licenses pre-split if they have a space.
        s/ (Apache|Artistic) ' License'? ' 2.0' /$0-2.0/;
        s/ 'General Public License ' (\d)       /GPL-$0.0-only/;
        s/ 'Public Domain'                      //;

        .split(/ ','? \s+ /).map({
            %license-map{$_}:exists ?? %license-map{$_} !! $_
        }).sort;
    }

    # Munge Raku version.
    %meta<perl> //= 6;
    %meta<perl> ~~ s/^ 'v'         //;
    %meta<perl> ~~ s/  '.'? '*'   $//;
    %meta<perl> ~~ s/  '.0'+      $//;
    %meta<perl> ~~ s/  '.PREVEIW' $//;  # Old Data::Record typo.
    %meta<perl> ~~ s/  '.PREVIEW' $//;
    %meta<perl> ~~ s/  '.^'       $//;
    %meta<perl> ~~ s/  '.'        $//;
    %meta<perl> ~~ s/  '+'        $//;

    # Changes. Prefer markdown to plain text.
    for <Changes.md.html CHANGELOG.md.html Changes.html ChangeLog.html> {
        my $html = try $dir.subst("/cache", "/cache/html").IO.add($_).slurp;

        # Use the first one with content.
        if $html.?chars {
            %meta<changes> = $html;
            last;
        }
    }

    # Readme. Prefer markdown to plain text.
    for <README.md.html README.markdown.html README.html readme.md.html> {
        my $html = try $dir.subst("/cache", "/cache/html").IO.add($_).slurp;

        # Use the first one with content.
        if $html.?chars {
            %meta<readme> = $html;
            last;
        }
    }

    my $url = %meta<support><source> // %meta<source-url>;

    # Munge GitHub & GitLab URLs into a consistent form.
    s{^
        [ 'git@' | < git http https ssh > '://' ]
        'www.'? $<host> = < github gitlab > '.com' <[ / : ]>
        $<user> = .*? '/' $<repo> = .*? '.git'?
    $} = "https://$<host>.com/$<user>/$<repo>" with $url;

    # Author meta is optional on zef, ensure we have a minimal author.
    %authors{$auth} //= :name(~$0) if $auth ~~ /^ 'zef:' (.+) /;

    # Normalize tags.
    %meta<tags> = ( %meta<tags> // [] )».lc.map({ %tag-mapping{$_} // $_ })
        .grep(/^ <[ a..z 0..9 ]>+ $/).unique.sort;

    $db.execute: 'SAVEPOINT insert_dist';

    {
        CATCH {
            when DB::Pg::Error {
                $db.execute: 'ROLLBACK TO SAVEPOINT insert_dist';
                .rethrow;
            }
        }

        $insert-author.execute: $auth, |.<date email name website>
            with %authors{$auth};

        my %dist = (
            :$eco, :@licenses, :$url,
            author_id => $auth,
            date      => $dir.add("date").slurp.DateTime,
            download  => try { $dir.add("download").slurp },
            downloads => %downloads{"%meta<name>:ver<*>:auth<$auth>"},
            provides  => %meta<provides>.keys.sort // [],
            size      => try { $dir.add("size").slurp },
            |<api auth changes description name perl readme tags version>.map:
                { $_ => %meta{$_} },
        );

        # Not every dist insert is "new", skip deps if we don't get an ID.
        my $id = $insert-dist.execute(|%dist.sort».value).value or next;

        # TODO Use Zef::Dist more, maybe replace %dist with Zef::Distribution?
        with try Zef::Distribution.new(|%meta) {
            $insert-dep.execute($id, |.sort».value) for
                |.build-depends-specs».spec-parts».push({ :phase<build>   }),
                      |.depends-specs».spec-parts».push({ :phase<runtime> }),
                 |.test-depends-specs».spec-parts».push({ :phase<test>    });
        }
    }
}

# Update dist info from Git{Hub,Lab}.
my %queries;

for $db.query(q:to/SQL/).arrays {
    SELECT DISTINCT url FROM dists WHERE url ~ '^https://git(hu|la)b\.com/'
SQL
    my ( $, $, $domain, $owner, $repo ) = .split: '/';

    my $id = 'i' ~ ++$;

    %queries{$domain}.push: $domain eq 'github.com'
        ?? Q:s{$id: repository(owner: "$owner" name: "$repo") { ...r }}
        !! Q:s{$id: project(fullPath: "$owner/$repo") { ...p }};
}

# Batch GraphQL queries to avoid hitting complexity limits or time-outs.
my @info = (
    |(|graphql 'github', Q:s:to/QUERY/ for %queries<github.com>.batch: 100),
        fragment r on Repository {
            issuesEnabled: hasIssuesEnabled
                           issues(states: OPEN) { count: totalCount }
                           pullRequests(states: OPEN) { count: totalCount }
                    stars: stargazerCount
                           url
        } { $_ }
    QUERY

    # https://docs.gitlab.com/ee/api/graphql/index.html#max-query-complexity
    |(|graphql 'gitlab', Q:s:to/QUERY/ for %queries<gitlab.com>.batch: 16),
        fragment p on Project {
                    issuesEnabled
            issues: issueStatusCounts { count: opened }
      pullRequests: mergeRequests(state: opened) { count }
             stars: starCount
               url: webUrl
        } { queryComplexity { score limit } $_ }
    QUERY
);

# Only use the issue count if issues are enabled.
.<issues> = .<issuesEnabled>:delete ?? .<issues><count> !! Nil for @info;

# Match the URL case-insensitively but update it to the canonical value.
$db.query(
    'WITH info AS (
        SELECT UNNEST($1::int[])  issues,
               UNNEST($2::int[])  pull_requests,
               UNNEST($3::int[])  stars,
               UNNEST($4::text[]) url
    ) UPDATE dists
         SET issues = info.issues,
      pull_requests = info.pull_requests,
              stars = info.stars,
                url = info.url
        FROM info
       WHERE dists.url ILIKE info.url',
    @info».<issues>,
    @info».<pullRequests>».<count>,
    @info».<stars>,
    @info».<url>,
);

$db.execute: 'REFRESH MATERIALIZED VIEW CONCURRENTLY distinct_dists';

# Try to resolve deps to paths.
# TODO: modules, no auth, version ranges, etc.

# dist(auth, name, ver) → auth/name?v=ver (specific version).
$db.execute: q:to/SQL/;
    UPDATE deps
       SET path = dists.slug || '?v=' || dists.version
      FROM dists
     WHERE deps.auth = dists.auth
       AND deps.from = 'Raku'
       AND deps.name = dists.name
       AND deps.ver  = dists.version
SQL

# dist(auth, name) → auth/name (latest version).
$db.execute: q:to/SQL/;
    UPDATE deps
       SET path = distinct_dists.slug
      FROM distinct_dists
     WHERE deps.auth = distinct_dists.auth
       AND deps.from = 'Raku'
       AND deps.name = distinct_dists.name
       AND deps.ver  = ''
SQL

$db.commit;
$db.execute: 'VACUUM ANALYZE authors, deps, distinct_dists, dists';

my @rows = $db.query(q:to/SQL/).arrays».subst: 'bytes', ' B';
    WITH tables AS (
        SELECT relname                                            "table",
               reltuples                                          "rows",
               pg_indexes_size(c.oid)                             index_size,
               COALESCE(pg_total_relation_size(reltoastrelid), 0) toast_size,
               pg_total_relation_size(c.oid)                      total_size
          FROM pg_class     c
          JOIN pg_namespace n ON relnamespace = n.oid
         WHERE nspname = 'public' AND relkind IN ('m', 'r')
    ) SELECT "table", "rows",
             pg_size_pretty(total_size - index_size - toast_size),
             pg_size_pretty(index_size),
             pg_size_pretty(toast_size),
             pg_size_pretty(total_size)
        FROM tables
    ORDER BY "table";
SQL

say '';
say '     table      │ rows  │ table size │ index size │ toast size │ total size ';
say '────────────────┼───────┼────────────┼────────────┼────────────┼────────────';
printf " %-14s │ %5d │ %10s │ %10s │ %10s │ %10s\n", |$_ for @rows;
say '';

sub get {
    state $ua = HTTP::Tiny.new :throw-exceptions;

    $ua.get($^url)<content>.decode;
}
