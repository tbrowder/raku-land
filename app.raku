use Cro::HTTP::Server;
use DateTime::Relative;
use Local::Middleware::Log;
use Local::Routes;
use MONKEY-TYPING;

# Disable output buffering.
.out-buffer = False for $*ERR, $*OUT;

# FIXME These augmentations exist to use from templates, find a better way!
augment class Int {
    method bytes {
        return self ~ ' bytes'                  if self ≤ 1024¹;
        return sprintf '%.1f KiB', self / 1024¹ if self ≤ 1024²;
        return sprintf '%.1f MiB', self / 1024²;
    }

    method comma { $.flip.comb(3).join(",").flip }

    method ordinal {
        given self % 10 {
            when $_ == 1 && self % 100 != 11 { 'st' }
            when $_ == 2 && self % 100 != 12 { 'nd' }
            when $_ == 3 && self % 100 != 13 { 'rd' }
                                     default { 'th' }
        }
    }
}

augment class Str {
    method pretty-url { $.subst: /^ 'http' 's'? '://' / }
}

.start with my $app = Cro::HTTP::Server.new:
    :before(Local::Middleware::Log.request)
    :after(Local::Middleware::Log.response)
    :host<0.0.0.0>
    :port<1337>
    :$application;

printf "Listening after %.2fs…\n", now - $*INIT-INSTANT;

react whenever signal(SIGINT) {
    say 'Stopping…';

    $app.stop;

    done;
}
