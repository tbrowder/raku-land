FROM golang:1.19.2-alpine

ENV CGO_ENABLED=0 GOPATH=

COPY go.mod go.sum ./

RUN go mod download

COPY badge ./badge
COPY gmark ./gmark

RUN go build -ldflags -s -trimpath -o / ./badge ./gmark

FROM rakuland/raku:2022.12 AS dev

ENV RAKUDOLIB lib

WORKDIR /app

RUN mkdir /cache && chown nobody:nobody /cache \
 && apk add --no-cache alpine-sdk libpq openssl-dev rsync

COPY META6.json ./

RUN zef --deps-only --/test install .

COPY --from=0 /badge /gmark /usr/local/bin/

FROM dev

COPY app.raku esbuild.json tag-mapping.toml ./
COPY bin                                    ./bin
COPY dist                                   ./dist
COPY lib                                    ./lib
COPY static                                 ./static
COPY svg                                    ./svg
COPY views                                  ./views

COPY --from=0 /go/badge /go/gmark bin/

# Pre-compile the libraries, massively helps startup speed.
RUN raku -c app.raku

CMD ["raku", "app.raku"]
